<?php
$current_page = "Hypnotherapy";
?>
<?php include("_header.php"); ?>

		<!-- Begin Header -->
		<div class="container">
			<div class="row">

				<?php include("_social.php"); ?>
				
				<?php include("_menu.php"); ?>
				
					<div class="span8 offset4 wrapper">
						
						<section id="contact-us" class="main show">

							<h1 class="h1">Hypnotherapy</h1>


							<div class="content">

								<div class="row-fluid">

									<div class="span4">
										<h2>What is Hypnotherarpy</h2>
										<hr/>
										<h3>Hypnotherapy is a painless and drug free way to make changes in your life.</h3>
									</div>
									
									<div class="span7 pull-right">
										<h4>It can be successfully applied to a wide variety of problems either as the primary therapy or as a valuable adjunct to other treatments, including:</h4><br/>
										<ul>
										<li>Smoking</li>
										<li>Weight Loss</li>
										<li>Stress/Anxiety/Pain Control</li> 
										<li>Insomnia</li>
										<li>Gambling</li>
										<li>Phobias</li>
										<li>Motivation</li>
										<li>Low Self-Esteem</li>
										</ul>
										<hr/>
										<p>Hypnosis is a state of deep relaxation that is natural and safe.  It is a state of mind between waking and sleeping, where a person is conscious, aware of physical sensations and surroundings, and yet much more receptive to therapeutic suggestions than he or she normally would be.</p>

<p>It is a state of mind in which many problems and conditions may be explored and treated.  Virtually anyone can be hypnotised - some more easily than others....it is a voluntary state...you are always in control and cannot be made to be hypnotised or act against your will.</p>
										
										<p>Call today for further information or to make an appointment.</p>
										    						
									</div>

								</div>

							</div>

						</section>


					</div>
				</div>
			</div>

<?php include("_footer.php"); ?>
