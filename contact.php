<?php
$current_page = "Contact";
?>
<?php include("_header.php"); ?>

		<!-- Begin Header -->
		<div class="container">
			<div class="row">

				<?php include("_social.php"); ?>
				
				<?php include("_menu.php"); ?>
				
					<div class="span8 offset4 wrapper">
						
						<section id="contact-us" class="main show">

							<h1 class="h1">Contact us</h1>


							<div class="content">

								<div class="row-fluid">

									<div class="span8">
										<h2>
											240 La Trobe Terrace,<br/>
											Geelong VIC 3220‎<br/>
											Australia
										</h2>
										<h2>
											<span class="fill140">Phone:</span> 03 5333 1565<br/>
											<span class="fill140">Freecall:</span> 1800 110 660<br/>
											<a class="contact_form"><span class="fill140">Email:</span> ljsimmons1@bigpond.com</a>
										</h2>
										    						
									</div>
									
								</div>

							</div>

						</section>


					</div>
				</div>
			</div>

<?php include("_footer.php"); ?>
