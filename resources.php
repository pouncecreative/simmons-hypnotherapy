<?php
$current_page = "Resources";
?>
<?php include("_header.php"); ?>

		<!-- Begin Header -->
		<div class="container">
			<div class="row">

				<?php include("_social.php"); ?>
				
				<?php include("_menu.php"); ?>
				
					<div class="span8 offset4 wrapper">
						
						<section id="contact-us" class="main show">

							<h1 class="h1">Resources</h1>


							<div class="content">

								<div class="row-fluid">

									<div class="span8">
										<h2>
											Follow the links to find out more.
										</h2>
										<ul>
											<li><a href="http://www.asch.com.au/" target="_blank">Australian Society of Clinical Hypnotherapists (ASCH)</a></li>
										</ul>
										    						
									</div>
									
								</div>

							</div>

						</section>


					</div>
				</div>
			</div>

<?php include("_footer.php"); ?>
