/* cachen Vars */
var body				= jQuery('body');
var loader				= jQuery('#loader');
var mask_shape			= jQuery('#mask-shape');
var lightwindow			= jQuery('#lightwindow');
var lightwindow_content = jQuery('#lightwindow-content');
var contact_form_ajax	= jQuery('.contact_form_ajax');
var contact_form		= jQuery('.contact-form');
var newsletter			= jQuery('.newsletter_form');
var newsletter_form_ajax= jQuery('.newsletter_form_ajax');
var newsletter_form		= jQuery('#newsletter');
var newsletter_objects	= jQuery('#newsletter input, #newsletter button');
var message_wrapper		= jQuery('.message-wrapper');
var message_wrapper_ok	= jQuery('.message-wrapper-ok');
var div_social			= jQuery('.social');
var fb_social			= jQuery('.fb_social');
var slideshow			= jQuery("#slideshow");
var $container			= jQuery('section ul.isotope');
var $container_portfolio			= jQuery('.isotope-portfolio');
var temp = 0;
var pt_carousel;
var timeout_control;

jQuery(document).ready(function() {



	mask_shape.switchClass( "unloaded", "loaded", 250, 'easeInOutQuint',  function() { 

		jQuery('header, section, footer').css({ 'opacity' : 1 });

		jQuery('header figure').addClass('animated delay01 fast fadeInUp').one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function(e) {

			jQuery('.btn-navbar').addClass('animated delay04 fadeIn');

			jQuery('header nav ul.nav li a:not(header nav ul.nav ul li a)').each(function(i){ /* Menus */
				jQuery(this).addClass('animated fast delay0'+ i +' fadeInLeft');
				temp = i;
			});
			jQuery('header nav ul.nav ul li a').each(function(i){ /* Submenus */
				jQuery(this).addClass('animated fast delay0'+ (i+3) +' fadeInLeft');
			});

			temp++;
			temp++;
			jQuery('.copyright').addClass('animated delay0'+ temp +' fadeIn');

			jQuery('.social .social-icons li').each(function(i){
				jQuery(this).addClass('animated fast delay0'+ i +' fadeInDown');
				temp = i;
			});
			temp++;
			jQuery('.social .twitterfeed').addClass('animated delay0'+ temp +' fadeIn');
		});

		jQuery('section h1').addClass('animated delay10 fadeInUp');
		jQuery('section h2, .flexslider, section .phone-accordion').addClass('animated delay12 fadeInUp');
		jQuery('section .content').addClass('animated delay14 fadeInUp');

		jQuery('section .content h2').addClass('animated delay16 fadeInRight');
		jQuery('section .content .subtitle').addClass('animated delay18 fadeIn');
		jQuery('.categories-filter').addClass('animated delay16 fadeInRight');

		if(!Modernizr.csstransitions)
		{
			jQuery('nav li a, .copyright').css({'opacity':'1'});
		}
	});


	/* Sub nav*/
	var nav_width = 320;
	var nav_drop  = 0;
	
	jQuery('.pt-mainmenu').hoverIntent(function(){
		nav_drop = nav_width + jQuery(this).find('.dropdown-menu').width() + 200;
		jQuery('.subnav-layer').stop().animate({'width': nav_drop}, 600, 'easeOutQuart');
	}, function(){
		jQuery('.subnav-layer').animate({'width': 0}, 400, 'easeInQuart');
	});

	jQuery('.pt-submenu').hoverIntent(function(){
		var w = nav_drop + jQuery(this).find('.dropdown-menu').width();
		jQuery('.subnav-layer').animate({'width': w}, 600, 'easeOutQuart');
	}, function(){
		jQuery('.subnav-layer').animate({'width': nav_drop}, 400, 'easeInQuart');
	});

	/* Templates Portfolio Categoreis Filter */
	jQuery('.categories-filter').hoverIntent(function(){
		jQuery(this).addClass('active');
	}, function(){
		jQuery(this).removeClass('active');
	});




	/* Remove fixed h1 on sidebar scroll */
	jQuery('.sidebar-blog, .no-sidebar, .portfolio-1 .portfolio-item-2, .portfolio-2 .portfolio-item-3, .portfolio-3 #portfolio-row-1').waypoint(function(direction)
	{
		jQuery('h1.fixed').removeClass('delay10 fadeInUp fadeIn fadeOut show hide');

		if (direction === 'down') {
			jQuery('h1.fixed').addClass('animated fadeOut').one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function(e) {
				jQuery('h1.fixed').addClass('hide');
			});
			if( jQuery.browser.msie && jQuery.browser.version < 9 )
			{
				jQuery('h1.fixed').slideUp('fast');
			}
		}
		else {
			jQuery('h1.fixed').addClass('animated fadeIn show');
			if( jQuery.browser.msie && jQuery.browser.version < 9 )
			{
				jQuery('h1.fixed').slideDown('fast');
			}
		}
	}, {offset:320});


	/* Contact Form lightwindow button */
	jQuery('.contact_form').click(function() {
		body.addClass('no-scroll');
		lightwindow_content.append(contact_form_ajax);

		lightwindow_content.find('h1, h2').removeClass();
		lightwindow_content.find('h1').addClass('animated delay05 fadeInUp');
		lightwindow_content.find('h2').addClass('animated delay10 fadeInUp');
		lightwindow_content.find('form').addClass('animated delay15 fadeInUp');
	
		lightwindow.fadeIn();
		lightwindow_content.addClass('show');

		contact_form_ajax.addClass('show').addClass('ie-show');
		mask_shape.switchClass( "loaded", "loaded-window", 750, 'easeInOutQuint', function() { 
			contact_form_ajax.animate({ 'opacity' : 1 }, 500);
		});
	});


	/* Contact Form lightwindow Close button */
	jQuery(document).on('click', '.contact_form_ajax a.close', function(event){
		contact_form_ajax.animate({ 'opacity' : 0 }, 500, function() { 
			mask_shape.switchClass( "loaded-window", "loaded", 750, 'easeInOutQuint', function() {
				contact_form_ajax.removeClass('show');
				message_wrapper.hide().removeClass('animated bounceInLeft');
				message_wrapper_ok.hide().removeClass('animated bounceInLeft');
				lightwindow_content.removeClass('show');
				lightwindow_content.find('form').removeClass('animated fadeOutRight hide delay15 fadeInUp hide');
				lightwindow.hide('fade');				
				body.removeClass('no-scroll');
				lightwindow_content.empty();
			});
		});
		return false;
	});


	/* Contacts Form */
	jQuery(document).on("submit", ".contact-form", function(){
		var form_data = jQuery(this).serialize();
		var name = jQuery('.contact-form input.pt_contact_name').attr('value');
		var email = jQuery('.contact-form input.pt_contact_email').attr('value');
		var subject = jQuery('.contact-form input.pt_contact_subject').attr('value');
		var message = jQuery('.contact-form textarea.pt_contact_message').attr('value');

		var form = jQuery(this);
		var message_ok = jQuery(this).parents('.container').find('.message-wrapper-ok');
		var message_error = jQuery(this).parents('.container').find('.message-wrapper');

		form.removeClass('delay15 fadeInUp');

		form_data += '&action=pt-ajax-contact-form';

		if (validateEmail(email) && name!='' && subject!='' && message!='' )
		{
			jQuery.post(config.contact_form_submit, form_data, function(data) {
				if (data=="true") {
					form.addClass('bounceOutRight').one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function(e) {
						form.addClass('hide');
						form.removeClass('bounceOutRight');
						message_ok.show().addClass('animated bounceInLeft');
					});
					
				} else {
					message_error.show().addClass('animated bounceInLeft');
				}
			});
		}
		else
		{
			message_error.show().addClass('animated bounceInLeft');
		}

		return false;
	});


	/* Mailing list lightwindow button*/
	newsletter.click(function() {
		newsletter_form_ajax.addClass('show').addClass('ie-show');
		mask_shape.switchClass( "loaded", "loaded-window", 750, 'easeInOutQuint', function() { 
			newsletter_form_ajax.animate({ 'opacity' : 1 }, 500);
		});
		return false;
	});

	/* Close Mailing list lightwindow button*/
	jQuery('.newsletter_form_ajax a.close').click(function() {
		newsletter_form_ajax.animate({ 'opacity' : 0 }, 500, function() { 
			mask_shape.switchClass( "loaded-window", "loaded", 750, 'easeInOutQuint', function() {
				newsletter_form_ajax.removeClass('show');
				message_wrapper.hide().removeClass('animated bounceInLeft');
			});
		});
	});



	
	/* Our Team */
	jQuery('.our-team-picture').hover(function() {
		jQuery(this).find('ul li').each(function(i){
			jQuery(this).addClass('animated fast delay0'+ (i+1) +' fadeInUp');
		});
	}, function(){
		jQuery(this).find('ul li').each(function(i){
			jQuery(this).removeClass('fadeInUp');
		});
	});




	/* LightWindow */
	jQuery('.popup').magnificPopup({
		fixedContentPos: false,
		fixedBgPos: true,
		overflowY: 'auto',
		closeBtnInside: true,
		preloader: false,
		midClick: true,
		removalDelay: 300,
		mainClass: 'my-mfp-slide-bottom'
	});

	/* Placeholder */
	jQuery('input, textarea').placeholder();



	jQuery('.pt_testimonial_wrapper > div').bxSlider({
		mode : 'fade',
		controls : false,
		slideSelector : 'div.pt_testimonial_slide',
		adaptiveHeight : true
	});


	if ( Modernizr.mq('(max-width: 320px)') )
	{
		pt_carousel('.sc_recent_works ul', 246, 1, 1, 16, false);
		pt_carousel('.sc_recent_posts ul', 246, 1, 1, 16, false);
	}

	else if ( Modernizr.mq('(max-width: 480px)') )
	{
		pt_carousel('.sc_recent_works ul', 246, 2, 2, 16, false);
		pt_carousel('.sc_recent_posts ul', 246, 2, 2, 16, false);
	}

	else
	{
		pt_carousel('.sc_recent_works ul', 246, 3, 3, 16, false);
		pt_carousel('.sc_recent_posts ul', 246, 3, 3, 16, false);
	}

	
	pt_carousel('.client-list ul', 115, 1, 6, 16, false);

	jQuery(".sc_recent_works .bx-wrapper .bx-pager").center(true);

	jQuery('.fold-button').origami();

	jQuery('.flexslider').hover(function(){
		jQuery(this).find('ol.flex-control-thumbs li').each(function(i){
			jQuery(this).addClass('animated fast delay0'+ (i) +' fadeInUp');
		});
	}, function(){
		jQuery(this).find('ol.flex-control-thumbs li').each(function(i){
			jQuery(this).removeClass('fadeInUp');
		});
	});


	var share_state=0;
	jQuery(document).on('click', '.nav-share a.share', function(event) {

		var fb_social = jQuery('.fb_social');
		share_state = !share_state; 

		if ( share_state ) {
			fb_social.removeClass('fadeOutUp').addClass('fadeInDown');
			if( jQuery.browser.msie && jQuery.browser.version < 9 )
			{
				fb_social.slideDown('fast');
			}
		}
		else{
			fb_social.removeClass('fadeInDown').addClass('fadeOutUp');
			if( jQuery.browser.msie && jQuery.browser.version < 9 )
			{
				fb_social.slideUp('fast');
			}
		}
		event.preventDefault();
	});


	jQuery(document).on('click', '.lightwindow', function(event){

		var file = jQuery(this).attr('href');

		lightwindow.fadeIn();
		
		lightwindow_content.addClass('show').fadeIn();
		body.addClass('no-scroll');
		
		jQuery.ajax({
			url: file
		}).done(function(data) {
			lightwindow_content.html(data);

			portfolio_slider();

			if( jQuery.browser.mozilla ) {
				if ( lightwindow.hasScrollBar() == true ) {
					jQuery('#lightwindow #mask-shape, #lightwindow .bg').css({'right': jQuery.scrollbarWidth()});
					lightwindow.css({'overflow-y':'scroll'})
				}
			 }

			try{
				FB.XFBML.parse();
			}catch(ex){}

		});
		return false;
	});

	jQuery(document).on('click', '.close-lightwindow', function(event){
		
		lightwindow_content.fadeOut('normal', function() {
			lightwindow.fadeOut('normal', function() {
				body.removeClass('no-scroll');
				lightwindow_content.html('').css({'margin-top' : 0});
				lightwindow.css({'height' : 'auto'});
				lightwindow_content.removeClass('show');
			});
		});
		return false;
	});


});



/* Detect Scroll */
jQuery.fn.hasScrollBar = function() { "use strict"; return this.get(0).scrollHeight > this.innerHeight(); }

/* Returns Scroll size */
jQuery.scrollbarWidth=function(){"use strict"; var a,b,c;if(c===undefined){a=jQuery('<div style="width:50px;height:50px;overflow:auto"><div/></div>').appendTo('body');b=a.children();c=b.innerWidth()-b.height(99).innerWidth();a.remove();}return c;};


/* validate email */ 
function validateEmail(a){ var b=/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; return b.test(a); }

/* Vertical Align */
jQuery.fn.center = function(parent) {      
	if (parent) {
		parent = this.parent();
	} else {
		parent = window;
	}
	this.css({
		"position": "absolute",
		"top": (((( this.outerHeight()) ) + jQuery(parent).scrollTop())   + "px")
	});

	return this;
}

function pt_carousel(selector, slideWidth, minSlides, maxSlides, slideMargin, controls)
{
	var parent = jQuery(selector).parent();

	if (parent.hasClass('sc_recent_posts_vertical'))
	{
		carousel_mode = 'vertical';
		minSlides = 2;
		maxSlides = 2;
	}

	else
	{
		carousel_mode = 'horizontal';
	}

	jQuery(selector).bxSlider({
		slideWidth: slideWidth,
		minSlides: minSlides,
		maxSlides: maxSlides,
		slideMargin: slideMargin,
		controls : controls,
		mode : carousel_mode
	});
}

