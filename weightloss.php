<?php
$current_page = "Weight Loss";
?>
<?php include("_header.php"); ?>

		<!-- Begin Header -->
		<div class="container">
			<div class="row">

				<?php include("_social.php"); ?>
				
				<?php include("_menu.php"); ?>
				
					<div class="span8 offset4 wrapper">
						
						<section id="contact-us" class="main show">

							<h1 class="h1">Weight Loss</h1>


							<div class="content">

								<div class="row-fluid">

									<div class="span4">
										<h2>Lose Weight<br/>and <em>Keep it Off</em></h2>
										<hr/>
										<h3>Do you want to feel better and look better?</h3>
									</div>
									
									<div class="span7 pull-right">
										<h3>Are you tired of ‘trying’ to be good only to find that it is becoming more difficult to maintain a weight that you are happy with?</h3>
										<h3>Are you ready to do something different to be that slimmer, happier person?</h3>
										<hr/>
										<p>Firstly, most diets are based on an eating regime that is difficult to maintain, so often a person can lose weight but only to find that it is temporary and the ‘roller coaster’ starts again and as the disappointment continues, the fear of failure increases.
Our question to you is….Are you ready to commit to CHANGE?  If you answer YES to this question then this program is for you.
</p>
										<p>With Hypnosis we are working with the subconscious mind, your pathway to success is about to open up.  Our 'Lose Weight With Hypnosis' is about giving you the tools to shed those unwanted kilos once and for all.  You can actually RELAX and lose weight, sound too good to be true, then just think how stressful dieting can be.</p>
										<p>Our Program will show you how to CHANGE THE WAY YOU THINK ABOUT FOOD AND YOUR BODY permanently, easily, comfortably.</p>
										
										<p>We recommend you take advantage of our ‘1 SESSION STARTER PROGRAM’ that includes information notes and a supporting CD to keep your mind focussed on your new habits . Follow up sessions are also available. Most people find that this easy relaxed approach will assist in changing a lifetime of outdated beliefs and eating patterns that are so often the obstacle to successful weight loss. </p>
										
										<p>Call today for further information or to make an appointment.</p>
										    						
									</div>
									
								</div>

							</div>

						</section>


					</div>
				</div>
			</div>

<?php include("_footer.php"); ?>
