<!-- Begin Section Social -->
<div class="social hidden-phone span12">

    <?php 
    /*<div class="twitterfeed pull-right visible-desktop">
    	<div class="row">
    		<div class="twitter-feed-icon span1"></div>
    		<div class="twitter-feed span3"></div>
    	</div>
    </div>
	*/ ?>
	
    <div class="social-icons span8 pull-right">
    	<!-- Social Profiles. -->
    	<ul class="social-profiles pull-right text-right unstyled inline">
    		<li class="newsletter animated fast delay00 fadeInDown"><a href="#" class="contact_form"></a></li>

    		<li class="sep animated fast delay06 fadeInDown"></li>

    		<li class="facebook"><a href="http://facebook.com/" target="_blank"></a></li>
    		<li class="linkedin"><a href="http://linkedin.com/" target="_blank"></a></li>

    		<?php
    		/*
    		<li class="twitter"><a href="https://twitter.com/" target="_blank"></a></li>
    		<li class="gplus"><a href="https://plus.google.com" target="_blank"></a></li>
    		<li class="pinterest"><a href="http://pinterest.com/" target="_blank"></a></li>
    		<li class="dribbble"><a href="http://dribbble.com/" target="_blank"></a></li>
    		<li class="github"><a href="https://github.com/" target="_blank"></a></li>
    		<li class="flickr"><a href="http://www.flickr.com/" target="_blank"></a></li>
    		<li class="vimeo"><a href="https://vimeo.com/" target="_blank"></a></li>
    		<li class="tumblr"><a href="https://www.tumblr.com/" target="_blank"></a></li>
    		<li class="stumbleupon"><a href="http://www.stumbleupon.com/" target="_blank"></a></li>
    		<li class="lastfm"><a href="http://www.last.fm/?" target="_blank"></a></li>
    		<li class="instagram"><a href="http://instagram.com/" target="_blank"></a></li>
    		<li class="skype"><a href="http://www.skype.com/" target="_blank"></a></li>
    		<li class="soundcloud"><a href="https://soundcloud.com/" target="_blank"></a></li>
    		*/ ?>
    	</ul>
    </div>
</div>
<!-- End Section Social -->