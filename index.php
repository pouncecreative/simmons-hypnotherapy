<?php
$current_page = "Home";
?>
<?php include("_header.php"); ?>



		<!-- Begin Header -->
		<div class="container">
			<div class="row">

				<?php include("_social.php"); ?>
				
				<?php include("_menu.php"); ?>
					
					
					<div class="span8 offset4 wrapper">
						
						<section id="welcome" class="main show">
						
							<?php
							$quotes = array(
								"You <em>can</em> make a change",
								"Reverse Old Habits",
								"Be <em>Positive</em>",
								"<em>Let go</em> of Negativity",
								"Be <em>Happy</em>",
								"Improve your <em>Health</em>"
							);
							$rand_keys = array_rand($quotes, 1);
							?>
							
							<h1 class="h1"><?php echo $quotes[$rand_keys]; ?>.......</h1>

							<div class="content">

								<div class="row-fluid">

									<div class="span4">
										<div class="text-left">
											<h2>Hypnotherapy is a <em>painless</em> &amp; <em>drug free</em> way to make changes in your life.</h2>
										</div>
									</div>

									<div class="span7 pull-right">
										<div class="align-left">
											<h3>Specialising in the techniques of Hypnosis to bring about change</h3>
											<hr/>
											<p>For over 15 years John & Lee Simmons have been helping people to <a href="quitsmoking.php">Stop Smoking</a>, <a href="weightloss.php">Lose Weight</a>, Sleep Well and counter balance the effects of <a href="stress_anxiety.php">Stress and Anxiety</a>.</p>
										</div>
									</div>
								</div>

								<br><hr/><br>

								<div class="row-fluid">

									<div class="span4">
										<div class="fold-button origami">
											<div class="kami">
												<span class="hi-icon hi-icon-users"></span>
											</div>
											<div class="kami origami-content">
												<span class="hi-icon hi-icon-users"></span>
												<h4><br/>For over 15 years<br/>John & Lee Simmons<br/>have been helping people.</h4>
											</div>
										</div>
									</div>

									<div class="span4">
										<div class="fold-button origami">
											<div class="kami">
												<span class="hi-icon hi-icon-thumbs-up"></span>
											</div>
											<div class="kami origami-content">
												<span class="hi-icon hi-icon-thumbs-up"></span>
												<h4><br/>Hypnotherapy can be<br/>successfully applied to a wide variety of problems.</h4>
											</div>
										</div>
									</div>

									<div class="span4">
										<div class="fold-button origami">
											<div class="kami">
												<span class="hi-icon hi-icon-mobile"></span>
											</div>
											<div class="kami origami-content">
												<span class="hi-icon hi-icon-mobile"></span>
												<h4><br/><a href="#" class="contact_form">Call for a booking</a><br/>or for further information.</h4>

											</div>
										</div>
									</div>

								</div>

							</div>
							
						</section>

					</div>
				</div>
			</div>

<?php include("_footer.php"); ?>
