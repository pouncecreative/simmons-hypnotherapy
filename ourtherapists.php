<?php
$current_page = "Our Therapists";
?>
<?php include("_header.php"); ?>

		<!-- Begin Header -->
		<div class="container">
			<div class="row">

				<?php include("_social.php"); ?>
				
				<?php include("_menu.php"); ?>
				
					<div class="span8 offset4 wrapper">
						
						<section id="contact-us" class="main show">

							<h1 class="h1">Our Therapists</h1>

							<div class="content">
								<div class="row-fluid">
								
									<div class="span6">
										
										<div class="row-fluid">
											<div class="span12">
												<div class="our-team-picture">
													<img src="img/temp_profile.png" alt="John Simmons" width="378" height="190" />
													<ul class="social-profiles gradient unstyled inline">
														<li class="linkedin">
															<a href="#" target="_blank"></a>
														</li>
													</ul>
												</div>
												<div class="our-team">
													<h3>
														John Simmons
													</h3>
													<small><em>Founder</em></small>
													<hr/>
													<p>
													John concentrates on the Geelong office and is also available in Ballarat.  In addition he travels around regional Victoria and interstate conducting successful <a href="quitsmoking.php">Quite Smoking</a> and <a href="weightloss.php">Weight Loss Programs</a>, so look out for the next time he is in your area.  John has now introduced the invaluable techniques of Hypnotherapy to assist people to Manage Pain and enjoy a more comfortable life.  
													</p>
												</div>
												
											</div>
										</div>
										
									</div>
									
									
									
									
									
									<div class="span6">
										
										<div class="row-fluid">
											<div class="span12">
												<div class="our-team-picture">
													<img src="img/temp_profile.png" alt="Lee Simmons" width="378" height="190" />
													<ul class="social-profiles gradient unstyled inline">
														<li class="linkedin">
															<a href="#" target="_blank"></a>
														</li>

														<li class="twitter">
															<a href="https://twitter.com/pixelthrone" target="_blank"></a>
														</li>
														
														<li class="facebook">
															<a href="https://www.facebook.com/pixelthrone" target="_blank"></a>
														</li>
														<li class="gplus">
															<a href="#" target="_blank"></a>
														</li>
														
														
													</ul>
												</div>
												<div class="our-team">
													<h3>
														Lee Simmons
													</h3>
													
													<small><em>Founder</em></small>
													<hr/>
													<p>
													Lee concentrates her efforts in Ballarat and Geelong, offering the region Hypnotherapy and Counselling services.  Lee specialises in <a href="quitsmoking.php">Quite Smoking</a> and <a href="weightloss.php">Weight Loss for Women</a>. Lee has a Masters Degree in Counselling and Psychotherapy and uses her combined skills of Counselling, Psychotherapy and Hypnotherapy to assist Women of all ages with their life issues.  
												</p>
													
												</div>
												
												
																						</div>
										</div>
										
									</div>

								</div>
							</div>


						</section>


					</div>
				</div>
			</div>

<?php include("_footer.php"); ?>
