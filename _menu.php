<header class="span4 nav-wrapper">
    <div class="row">
    	<figure class="offset1 span2">
    			<h2 class="typebree">Simmons Hypnotherapy</h2>
    	</figure>

    	</div>
    	<div class="row">
    		<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">MENU</a>

			<?php
			$active = "current-menu-item active";
			?>
			<!-- Begin Navigation -->
			<nav class="offset1 span2 nav-collapse collapse" id="navigation">
			    <ul class="nav">
			    
			    	<li class="<?php if ($current_page == "Home") echo $active; ?>">
			    		<a href="/" class="menu1 dropdown-toggle">Home</a>
			    	</li>
			    	
			    	
			    	<li class="<?php if ($current_page == "Quit Smoking" || $current_page == "Weight Loss" || $current_page == "Stress Anxiety" || $current_page == "Pain Control") echo $active; ?> dropdown-submenu pt-mainmenu">
			    		<a href="#" data-toggle="dropdown" data-target="#"  class="menu7 dropdown-toggle">Services <span class="caret"></span></a>
			    		<ul class="dropdown-menu">
			    			<li<?php if ($current_page == "Quit Smoking") echo " class=\"$active\""; ?>><a href="quitsmoking.php" class="menu8">Quit Smoking</a></li>
			    			<li<?php if ($current_page == "Weight Loss") echo " class=\"$active\""; ?>><a href="weightloss.php" class="menu9">Weight Loss</a></li>
			    			<li<?php if ($current_page == "Stress Anxiety") echo " class=\"$active\""; ?>><a href="stress_anxiety.php" class="menu10">Stress / Anxiety</a></li>
			    			<li<?php if ($current_page == "Pain Control") echo " class=\"$active\""; ?>><a href="paincontrol.php" class="menu11">Pain Control</a></li>
			    		</ul>
			    	</li>
			    	
			    	
			    	<li class="<?php if ($current_page == "Hypnotherapy" || $current_page == "Our Therapists") echo $active; ?> dropdown-submenu pt-mainmenu">
			    		<a href="#" data-toggle="dropdown" data-target="#" class="menu14 dropdown-toggle">About <span class="caret"></span></a>
			    		<ul class="dropdown-menu">
			    			<li<?php if ($current_page == "Hypnotherapy") echo " class=\"$active\""; ?>><a href="hypnotherapy.php" class="menu15">Hypnotherapy</a></li>
			    			<li<?php if ($current_page == "Our Therapists") echo " class=\"$active\""; ?>><a href="ourtherapists.php" class="menu16">Our Therapists</a></li>
			    		</ul>
			    	</li>
			    	
					<li class="<?php if ($current_page == "News") echo $active; ?>"><a href="news.php" class="menu16">News</a></li>


					<li class="<?php if ($current_page == "Resources") echo $active; ?>"><a href="resources.php" class="menu17">Resources</a></li>
					
					
			    	<li class="<?php if ($current_page == "Contact") echo $active; ?>"><a href="contact.php" class="menu20">Contact</a></li>
			    	
			    </ul>
			
			
			</nav>
			<!-- End Navigation -->


	</div>
	<div class="subnav-layer"></div>
</header>
