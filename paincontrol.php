<?php
$current_page = "Pain Control";
?>
<?php include("_header.php"); ?>

		<!-- Begin Header -->
		<div class="container">
			<div class="row">

				<?php include("_social.php"); ?>
				
				<?php include("_menu.php"); ?>
				
					<div class="span8 offset4 wrapper">
						
						<section id="contact-us" class="main show">

							<h1 class="h1">Pain Control</h1>


							<div class="content">

								<div class="row-fluid">

									<div class="span4">
										<h2>Do you want to feel better and look better?</h2>
									</div>
									
									<div class="span7 pull-right">
										<p>Pain Control is one of the most challenging yet rewarding aspects of our practice: helping people to cope with the debilitating effects of chronic pain. Pain is very real. By assisting a person to achieve a better state of mind can have a positive, significant impact on their ability to keep pain from overtaking their life. A combination of deep relaxation and specific mental imaging can help manage pain. </p>
										
										<p>Because pain is a by-product of a specific medical condition, it is important to work in conjunction with your doctor; hypnotherapy is not intended to treat illness. Ask you doctor if hypnotherapy might make sense as part of your medical program.</p>
										
										<p>Call today for further information or to make an appointment.</p>
										    						
									</div>
									
								</div>

							</div>

						</section>


					</div>
				</div>
			</div>

<?php include("_footer.php"); ?>
