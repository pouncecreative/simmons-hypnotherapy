<?php
$current_page = "News";
?>
<?php include("_header.php"); ?>

		<!-- Begin Header -->
		<div class="container">
			<div class="row">

				<?php include("_social.php"); ?>
				
				<?php include("_menu.php"); ?>
				
					<div class="span8 offset4 wrapper">
						
						<section id="contact-us" class="main show">

							<h1 class="h1">News</h1>


							<div class="content">

								<div class="row-fluid">

									<div class="span8">
										<h2>Private Sessions</h2>
										<h3>GEELONG & BALLARAT</h3>
										<p>Monday to Friday. (Weekend and after hours appointments available).</p>
										
										<br/>
										<hr/>
										<br/>
										
										<h2>Calendar</h2>
										<h3>AUGUST</h3>
										<p>Mildura: Wed 21 Thur 22</p>
										
										<br/>
										<h3>OCTOBER</h3>
										<p>Traralgon: Tue/Wed 22 & 23</p>
										<p>Wonthaggi: Thur 24</p>
										<p>Leongatha: Fri 25</p>
										<p>Warragul: Fri/Sat 25 & 26</p>

										<br/>
										<h3>NOVEMBER</h3>
										<p>Echuca: Wed/Thu 13 & 14</p>

									</div>
									
								</div>

							</div>

						</section>


					</div>
				</div>
			</div>

<?php include("_footer.php"); ?>
