<?php
$current_page = "Stress Anxiety";
?>
<?php include("_header.php"); ?>

		<!-- Begin Header -->
		<div class="container">
			<div class="row">

				<?php include("_social.php"); ?>
				
				<?php include("_menu.php"); ?>
				
					<div class="span8 offset4 wrapper">
						
						<section id="contact-us" class="main show">

							<h1 class="h1">Stress &amp; Anxiety</h1>


							<div class="content">

								<div class="row-fluid">

									<div class="span4">
										<h2><em>Say Goodbye</em> to Stress/Anxiety with Hypnosis</h2>
									</div>
									
									<div class="span8">
										<p>As humans we all encounter stressful situations as we move through the challenges we face in every day life. How individuals manage these situations is the all important factor.</p>
										
										<p>John & Lee Simmons use Hypnotherapy techniques that help you to negotiate these challenges in a more comfortable and relaxed manner. Our <strong>'Say Goodbye to Stress/Anxiety with Hypnosis'</strong> Program will assist you to program your subconscious mind to respond with a more positive outlook and achieve a better quality of life. </p>
										<p>Call today for further information or to make an appointment.</p>
										    						
									</div>
									
								</div>
									
								</div>

							</div>

						</section>


					</div>
				</div>
			</div>

<?php include("_footer.php"); ?>
