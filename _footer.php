			<footer>
				<div class="container">

					<div class="twitterfeed visible-tablet">
						<div class="row">
							<div class="twitter-feed"></div>
						</div>
						<div class="row">
							<div class="twitter-icon">&#62217;</div>
						</div>
					</div>

					<div class="row">
						<div class="copyright offset1 span3">
							<p class="opcty06">&copy; <?php echo date('Y') ?></p>
							<a href="http://www.asch.com.au/" target="_blank" class="o_logo"><img src="img/logos/asch_logo.png" alt="Australian Society of Clinical Hypnotherapists (ASCH)" /></a>
						</div>
					</div>

				</div>
			</footer>





			<div class="contact_form_ajax hide">
			    
			    <a class="close"></a>
			    <div class="container">

			    	<div class="row">
			    		<div class="offset2 span8">
			    			<h1>Send us an email</h1>
			    		</div>
			    	</div>

			    	<div class="row">

			    		<div class="span2 offset1">
			    			<div class="message-wrapper">
			    				<div class="message">
			    					There was a problem while sending your message. Please try again.
			    				</div>
			    			</div>
			    		</div>

			    		<div class="span4 offset1 message-wrapper-ok">
			    			<div class="message-ok">
			    				Thank you for your contact. We'll be in touch soon.
			    			</div>
			    		</div>

			    		<div class="span6">

			    			<form method="post" action="#" class="row contact-form">
			    				<input type="text" required="" name="pt_contact_name" placeholder="Your Name (Required)" class="input-block-level required pt_contact_name">
			    				<input type="email" required="" name="pt_contact_email" placeholder="Your Email (Required)" class="input-block-level required email pt_contact_email"> 
			    				<textarea required="" rows="3" name="pt_contact_message" placeholder="Your Message (Required)" class="input-block-level required pt_contact_message"></textarea>
			    				<button>Submit</button>
			    			</form>

			    			<div id="errorContainer"></div>

			    		</div>

			    	</div>

			    </div>

			</div>





<?php /*
			<div id="lightwindow">
				<div id="lightwindow-content" class="hide"></div>
			</div>

			<!-- Begin Mailing List -->
			<div class="mailing-list-ajax newsletter_form_ajax hide">
				<a class="close"></a>
				<div class="container">
					<div class="row">
						<div class="offset2 span8">
							<h1 class="animated delay05 fadeInUp">Join our<br>mailing list</h1>
							<h2 class="animated delay010 fadeInUp">to stay up date</h2>
						</div>
					</div>
					<div class="row">
						<div class="span2 offset1">
							<div class="message-wrapper">
								<div class="message">Please enter a valid e-mail</div>
								<div class="message-arrow"></div>
							</div>
						</div>
						<div class="span6">
							<form method="post" id="newsletter" action="newsletter.php" class="animated delay15 fadeInUp">
								<input type="text" class="input-block-level" name="email" placeholder="Your e-mail">
								<button>- show me your world</button>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- End Mailing List -->
*/ ?>





			<div id="fb-root"></div>



			<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        	<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.8.3.min.js"><\/script>')</script>
			<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
			<script src="js/vendor/retina-2x/retina.js"></script>
			<script src="js/plugins.js"></script>
			<script src="js/main.js"></script>

	</body>
</html>