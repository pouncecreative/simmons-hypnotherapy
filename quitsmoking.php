<?php
$current_page = "Quit Smoking";
?>
<?php include("_header.php"); ?>

		<!-- Begin Header -->
		<div class="container">
			<div class="row">

				<?php include("_social.php"); ?>
				
				<?php include("_menu.php"); ?>
				
					<div class="span8 offset4 wrapper">
						
						<section id="contact-us" class="main show">

							<h1 class="h1">Quit Smoking</h1>


							<div class="content">

								<div class="row-fluid">

									<div class="span4">
										<h2>Stop Smoking and Start Living……<br/><em>Today</em>!</h2>
										<hr/>
										<h3>Do you want to be FREE?</h3>
										<h3>Do you want to BREATHE EASILY?</h3>
									</div>
									
									<div class="span7 pull-right">
										<p>Do you want to have more MONEY?</p>
										<p>Do you want to be more RELAXED? </p>
										<p>Are you tired of having your life dominated by SMOKING? </p>
										
										<p>If you have answered YES to 2 or more of these questions then it’s time to Stop Smoking and take charge over the Smoking habit. </p>
										
										<p>Our ‘Quit Smoking with Hypnosis’ program has helped thousands to live their life SMOKE FREE. Perhaps you have already tried to stop smoking but have given in to a moment and have found that you have become a smoker again. We have seen this happen to many people and have helped them to overcome this difficulty and to become SMOKE FREE as a permanent way of life. </p>
										
										<p>BE PART OF OUR 90-95% SUCCESS RATE</p>
										
										
										<p>Our program cost includes a CD designed for use in the first few days to assist the new non-smoker to stay focused and relaxed.  The non-smoking habit is then firmly established in the subconscious mind.</p>
										    						
									</div>
									
									
									
								</div>

							</div>

						</section>


					</div>
				</div>
			</div>

<?php include("_footer.php"); ?>
